import pandas as pd
import argparse
from pathlib import Path
import numpy as np

# create the top-level parser
parser = argparse.ArgumentParser()
parser.add_argument("--sgbtaxo", help="SGB taxonomy (GTDB format)", default=Path("mpa_vOct22_CHOCOPhlAnSGB_202212_SGB2GTDB.tsv.gz"),
                    type=Path)
parser.add_argument("--keepZeros", help="Do not remove SGB that have a null count in the final matrix",
                    action="store_false")
parser.add_argument('metaphlan_SBG_agg', type=Path, nargs='+')
parser.add_argument('output_matrix', type=Path, default="matrix.tsv", help="Combine all metaphlan SGB data into a contengency matrix")
parser.add_argument('output_taxo_table', type=Path, default="taxonomy.tsv", help="If sgbtaxo available, output the taxonomy of existing SGB")
args = parser.parse_args()
dataframes = [pd.read_csv(f, delimiter="\t") for f in args.metaphlan_SBG_agg]

# Now you can concatenate them into one DataFrame
df = dataframes[0]
for dataframe in dataframes[1:]:
    df = pd.merge(df, dataframe, on='SGB')


# Remove rows with only zeros
if args.keepZeros:
    df = df[df.select_dtypes(include=[np.number]).sum(axis=1) != 0]

df.to_csv(args.output_matrix, sep="\t", index=False)

if args.sgbtaxo:
    taxo = pd.read_csv(args.sgbtaxo, compression="gzip", delimiter="\t", names=["SGB", "Annotation"])
    # print(taxo)
    taxo = taxo[taxo['SGB'].isin(df['SGB'])]
    taxo['Annotation'] = taxo['Annotation'].str.replace(r'\w__', '', regex=True)
    # Split 'Annotation' into multiple columns
    # print(taxo['Annotation'].str.split(';', expand=True))
    taxo[['Domain', 'Phylum', 'Class', 'Order', 'Family', 'Genus', 'Species']] = taxo['Annotation'].str.split(';', expand=True)
    # Add 'marker_SGB' column
    taxo['marker_SGB'] = taxo['SGB'] + '_' + taxo['Annotation'].apply(lambda x: [i for i in x.split(';') if i][-1])
    del taxo['Annotation']
    taxo.to_csv(args.output_taxo_table, sep="\t", index=False)
