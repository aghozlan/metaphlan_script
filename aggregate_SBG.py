#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html
import argparse
import pandas as pd
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument("metaphlan_file", type=Path)
parser.add_argument("metaphlan_len", type=Path)
parser.add_argument("sample_name", type=str)
parser.add_argument("output_file", type=Path)
args = parser.parse_args()

core_content = pd.read_csv(args.metaphlan_file, delimiter="\t", names=["gene", "count"], low_memory=False )
metaphlan_len = pd.read_csv(args.metaphlan_len, delimiter="\t", compression="gzip", names=["gene", "len"])
core_content = core_content.merge(metaphlan_len[["gene", "len"]],on="gene", how="inner")
core_content["SGB"] = core_content['gene'].str.split('|').str[-1]
#print(core_content)
core_content['len'] = pd.to_numeric(core_content['len'])
core_content['count'] = pd.to_numeric(core_content['count'])
core_content[args.sample_name] = core_content["count"]/core_content["len"]*1000
del core_content["len"]
del core_content["count"]
del core_content["gene"]
#print(core_content)
res = core_content.groupby("SGB")[args.sample_name].mean()
#res = core_content.groupby("SGB").agg("mean")
res = res.round(0).astype(int)
#print(res)
res.to_csv(args.output_file,sep="\t")
